/**
 * @file
 * JS for uwvideo embedding.
 *
 * Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license.
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;

  function loadValue(node) {
    if (node.hasAttribute(this.id)) {
      var value = node.getAttribute(this.id);
      this.setValue(value);
    }
  }

  function commitValue_uw_video_url(node) {
    var value = this.getValue();
    value = CKEDITOR.uw_video_embed.clean_youtube_url(value);
    node.setAttribute(this.att || this.id, value);
  }

  CKEDITOR.dialog.add('uw_video_embed', function (editor) {
    var uw_videoLang = editor.lang.uw_video,
      commonLang = editor.lang.common;
    return {
      title : uw_videoLang.title,
      onShow : function () {
        // Clear previously saved elements.
        this.fakeImage = this.uw_videoNode = null;
        this.insertMode = true;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'uwvideo') {
          this.fakeImage = fakeImage;

          var uw_videoNode = editor.restoreRealElement(fakeImage);
          this.uw_videoNode = uw_videoNode;

          this.setupContent(uw_videoNode);
        }
      },
      onOk : function () {
        errors = '';
        if (!CKEDITOR.uw_video_embed.youtube_regex.test(this.getValueOf('info','href'))) {
          errors += "You must paste a valid YouTube URL.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          editor.insertMode = true;

          var uw_videoNode = new CKEDITOR.dom.element('uwvideo'), div = editor.document.createElement('div');
          this.commitContent(uw_videoNode);

          var newFakeImage = editor.createFakeElement(uw_videoNode, 'uw_video', 'uwvideo', false);
          newFakeImage.addClass('uw_video');
          if (this.fakeImage) {
              newFakeImage.replace(this.fakeImage);
              editor.getSelection().selectElement(newFakeImage);
          }
          else {
              editor.insertElement(newFakeImage);
          }
        }
      },
      contents : [
        {
          id : 'info',
          label : commonLang.generalTab,
          accessKey : 'I',
          elements : [
            {
              type : 'vbox',
              padding : 0,
              children : [
                {
                  id : 'href',
                  type : 'text',
                  label : uw_videoLang.url,
                  required : true,
                  setup : loadValue,
                  commit : commitValue_uw_video_url
                }
              ]
            }
          ]
        }
      ]
    };
  });
}());
