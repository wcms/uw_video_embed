/**
 * @file
 * JS for uwvideo embedding.
 *
 * Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license.
 */

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

(function () {
  CKEDITOR.plugins.add('uw_video_embed', {
    requires : ['dialog', 'fakeobjects'],
    init : function (editor) {
      CKEDITOR.lang.en.uw_video = {toolbar: 'Embed Video', title: 'Video Properties', url: 'Paste YouTube URL', noUrl: 'You must paste a valid YouTube URL'};
      var pluginName = 'uw_video_embed',
      lang = editor.lang.uw_video;

      // Configure DTD.
      CKEDITOR.dtd.uwvideo = {};
      // Define element as "block level" so the editor doesn't wrap it in 'p'.
      CKEDITOR.dtd.$block.uwvideo = 1;
      CKEDITOR.dtd.body.uwvideo = 1;
      // Make uwvideo be a self-closing tag.
      CKEDITOR.dtd.$empty.uwvideo = 1;

      // Make sure fake object has a name.
      CKEDITOR.lang.en.fakeobjects.uwvideo = 'YouTube video';

      CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/video_embed.js');
      editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));

      // Values used in this file and in video_embed.js.
      CKEDITOR.uw_video_embed = {};
      /*
      This regex must match the one in uw_video_embed.module.
      http://www.youtube.com/watch?v=u7F2rlRwDN8
      http://www.youtube.com/watch?v=u7F2rlRwDN8?rel=0
      https://www.youtube.com/watch?feature=player_embedded&v=u7F2rlRwDN8
      http://youtu.be/u7F2rlRwDN8
      http://www.youtube-nocookie.com/embed/u7F2rlRwDN8?rel=0
      https://www.youtube.com/watch?v=u7F2rlRwDN8&t=1m10s
      Obsolete:
      http://www.youtube.com/user/uwaterloo#p/a/009A50BD374AA3CA/1/aSggrq08mG4
       */
      CKEDITOR.uw_video_embed.youtube_regex = /^https?:\/\/(?:www\.youtube(?:-nocookie)?\.com\/(watch\?(?:\S+&)?v=|embed\/|.+#.+\/)|youtu\.be\/)([\w\-]{11})(?:[#&?]\S*)?$/;
      CKEDITOR.uw_video_embed.clean_youtube_url = function (url) {
        var url_parse = CKEDITOR.uw_video_embed.youtube_regex.exec(url);

        if (url_parse) {
          // Parse out playback start time if present.
          var start = '';
          var start_regex = /\?(?:.+&(?:amp;)?)?t=([0-9]+m)?([0-9]+s?)/;
          var start_parse = start_regex.exec(url);
          if (start_parse) {
            start = '&t=' + (start_parse[1] || '') + start_parse[2];
          }
          // Parse out playlist if present.
          var playlist = '';
          var playlist_regex = /\?(?:.+&(?:amp;)?)?list=([A-Za-z0-9-_]+)/;
          var playlist_parse = playlist_regex.exec(url);
          if (playlist_parse) {
            playlist = '&list=' + playlist_parse[1];
          }
          url = 'https://www.youtube.com/watch?v=' + url_parse[2] + start + playlist;
        }
        else {
          // "Blank out" unexpected URLs.
          url = '';
        }

        return url;
      };

      editor.ui.addButton('uw_video_embed', {
        label : lang.toolbar,
        command : pluginName,
        icon: this.path + 'images/youtube-favicon.png'
      });

      editor.on('doubleclick', function (evt) {
        var element = evt.data.element;

        // Store the element as global variable to be used in onShow of dialog, when double clicked.
        doubleclick_element = element;

        if (element.is('img') && element.data('cke-real-element-type') === 'uwvideo') {
          evt.data.dialog = 'uw_video_embed';
        }
      });

      // If the contextmenu plugin is loaded, register the listeners.
      if (editor.contextMenu) {
        editor.contextMenu.addListener(function (element, selection) {
          if (element && element.is('img') && element.data('cke-real-element-type') === 'uwvideo') {
            return { uw_video_embed : CKEDITOR.TRISTATE_OFF };
          }
        });
      }

      CKEDITOR.addCss(
          'img.uw_video' +
          '{' +
            'background-image: url(' + CKEDITOR.getUrl(this.path + 'images/youtube-logo-black.png') + ');' +
            'background-position: center center;' +
            'background-repeat: no-repeat;' +
            'background-color: #000;' +
            'width: 100%;' +
            'height: 100%;' +
            'margin: 0 0 10px 0;' +
            'margin-left: auto;' +
            'margin-right: auto;' +
          '}' +
          // Definitions for wide width.
          '.uw_tf_standard_wide p img.uw_video {' +
            'max-height: 466px;' +
          '}' +
          '.uw_tf_standard_wide .col-50 img.uw_video {' +
            'max-height: 233px;' +
          '}' +
          '.uw_tf_standard_wide .col-33 img.uw_video {' +
            'max-height: 165px;' +
            'background-size: 100%' +
          '}' +
          '.uw_tf_standard_wide .col-66 img.uw_video {' +
            'max-height: 311px;' +
          '}' +
          '.uw_tf_standard_wide .threecol-33 img.uw_video {' +
            'max-height: 165px;' +
            'background-size: 100%' +
          '}' +
          // Definitions for standard width.
          '.uw_tf_standard p img.uw_video {' +
            'max-height: 311px;' +
          '}' +
          '.uw_tf_standard .col-50 img.uw_video {' +
            'max-height: 165px;' +
            'background-size: 100%' +
          '}' +
          '.uw_tf_standard .col-33 img.uw_video {' +
            'max-height: 121px;' +
            'background-size: 100%' +
          '}' +
          '.uw_tf_standard .col-66 img.uw_video {' +
            'max-height: 232px;' +
          '}' +
          '.uw_tf_standard .threecol-33 img.uw_video {' +
            'max-height: 121px;' +
            'background-size: 100%' +
          '}'
          // Definitions for sidebar for facebook, twitter and mailman, add later...
        );

      editor.addMenuItems({
        uw_video_embed : {
          label : lang.title,
          icon: this.path + 'images/youtube-favicon.png',
          command : pluginName,
          group : 'image',
          order : 1
        }
      });
    },
    afterInit : function (editor) {
      if (editor.dataProcessor.dataFilter) {
        editor.dataProcessor.dataFilter.addRules({
          elements : {
            uwvideo : function (element) {
              element.attributes.href = CKEDITOR.uw_video_embed.clean_youtube_url(element.attributes.href);
              return editor.createFakeParserElement(element, 'uw_video', 'uwvideo', false);
            }
          }
        });
      }
    }
  });
}());
